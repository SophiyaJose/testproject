﻿using Microsoft.AppCenter.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkProject
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Green : ContentPage
    {
        private readonly List<string> _letter = new List<string>
        {
        "A","B","C","D","E","F","G"
        };
        public Green ()
		{
			InitializeComponent ();
            Analytics.TrackEvent("Green Page");
            MainList.ItemsSource = _letter;
        }
        public void Select_Clicked(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            _show.Text = e.SelectedItem.ToString();
            Analytics.TrackEvent(e.SelectedItem.ToString()+" -Item Selected");

        }
    }
}