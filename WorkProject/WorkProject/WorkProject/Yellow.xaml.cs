﻿using Microsoft.AppCenter.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkProject
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Yellow : ContentPage
	{
		public Yellow ()
		{
			InitializeComponent ();
            Analytics.TrackEvent("Yellow Page");
        }
        private void SwitchToggled(object sender, ToggledEventArgs args)
        {
            _colorBox.BackgroundColor = args.Value ? Color.Red : Color.Blue;
            Analytics.TrackEvent("Switch Toggled");

        }
    }
}