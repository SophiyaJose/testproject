﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin;
using Microsoft.AppCenter.Analytics;

namespace WorkProject
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        private void PinkClicked(object sender, EventArgs e)
        {
            Analytics.TrackEvent("Pink Clicked");

        }
        private void GreenClicked(object sender, EventArgs e)
        {
            Analytics.TrackEvent("Green Clicked");

        }
        private void YellowClicked(object sender, EventArgs e)
        {

            Analytics.TrackEvent("Yellow Clicked");
        }
        private void BlueClicked(object sender, EventArgs e)
        {

            Analytics.TrackEvent("Blue Clicked");
        }
        private async void PinkButtonClicked(object sender, EventArgs e)
        {

            await Navigation.PushAsync(new Pink());
            // Analytics.TrackEvent("Pink Page");
        }
        private async void GreenButtonClicked(object sender, EventArgs e)
        {
            //Analytics.TrackEvent("Video clicked", withProperties: ["Category" : "Music", "FileName" : "favorite.avi"])

            Analytics.TrackEvent("Video clicked", new Dictionary<string, string> {{ "Category", "Sport" },{ "FileName", "running.avi"}});
            await Navigation.PushAsync(new Green());
            // Analytics.TrackEvent("Green Page");
        }
        private async void YellowButtonClicked(object sender, EventArgs e)
        {

            await Navigation.PushAsync(new Yellow());
            // Analytics.TrackEvent("Yellow Page");
        }
        private async void BlueButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Blue());
            // Analytics.TrackEvent("Blue Page");
        }
       
    }
}
