﻿using Microsoft.AppCenter.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkProject
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}
        private async void LoginClicked(object sender, EventArgs e)
        {
            if (_usrEntry.Text == "sophiya" && _pswdEntry.Text == "123")
            {
                Analytics.TrackEvent(_usrEntry.Text + "-User Login");
                await Navigation.PushAsync(new MainPage());
            }
            else if (_usrEntry.Text == "gisha" && _pswdEntry.Text == "abc")
            {
                Analytics.TrackEvent(_usrEntry.Text + "-User Login");
                await Navigation.PushAsync(new MainPage());
            }
            else
            {
                await DisplayAlert("Fail", "Check your Username and Password", "OK");
            } 
        }
    }
}