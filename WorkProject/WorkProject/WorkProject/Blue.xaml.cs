﻿using Microsoft.AppCenter.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Blue : ContentPage
    {
        private readonly List<string> _number = new List<string>
        {
        "1","2","3","4","5"
        };
        public Blue()
        {
            InitializeComponent();
            Analytics.TrackEvent("Blue Page");
            Number.ItemsSource = _number;
            Number.SelectedIndex = -1;
        }

        public void Selected_IndexChanged(object sender, EventArgs e)
        {
            int num = Convert.ToInt16(Number.SelectedItem);
            Analytics.TrackEvent( num+ " -Number Selected");

        }
       
    }
}